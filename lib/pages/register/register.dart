import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_hmi/common/utils/log.dart';
import 'package:get/get.dart';

class RegisterPage extends StatelessWidget {
  const RegisterPage({super.key});

  @override
  Widget build(BuildContext context) {
    print(Get.arguments);
    // TODO: implement build
    return const Scaffold(
        body: Padding(
      padding: EdgeInsetsDirectional.fromSTEB(40, 40, 40, 20),
      child: Stack(
        alignment: Alignment.topCenter,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TextField(
                decoration: InputDecoration(hintText: "请输入用户名"),
              ),
              TextField(
                decoration: InputDecoration(hintText: "请输入密码"),
              ),
            ],
          )
        ],
      ),
    ));
  }
}
