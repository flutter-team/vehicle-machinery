import 'package:get/get.dart';

class ChatUserState {
  final _chatLogList = [].obs;
  set chatLogList(value) => _chatLogList.value = value;
  List get chatLogList => [..._chatLogList];

  ChatUserState() {
    ///Initialize variables
  }
}
