import 'package:flutter_hmi/common/constant/router_param.dart';
import 'package:flutter_hmi/common/constant/router_tag.dart';
import 'package:get/get.dart';

import 'logic.dart';

class ChatUserBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => ChatUserLogic(),
      tag: "${RouterTag.chatUser}${Get.parameters[RouterParam.username]}",
    );
  }
}
