import 'package:get/get.dart';

class ApplicationState {
  final _selectedPageIndex = 0.obs;
  set selectedPageIndex(value) => _selectedPageIndex.value = value;
  int get selectedPageIndex => _selectedPageIndex.value;

  var _airNum = 20;
  set airNum(value) => _airNum = value;
  int get airNum => _airNum;

  ApplicationState() {
    ///Initialize variables
  }
}
