import 'package:flutter/material.dart';
import 'package:flutter_hmi/pages/application/pages/contacts/view.dart';
import 'package:flutter_hmi/pages/application/pages/find/view.dart';
import 'package:flutter_hmi/pages/application/pages/user/view.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';

import '../../common/style/theme.dart';
import '../../common/widget/app_bar/app_bar.dart';
import 'logic.dart';
import 'pages/message/view.dart';

var currentIndex = 0;

class ApplicationPage extends GetView<ApplicationLogic> {
  const ApplicationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Scaffold(
          body: Row(
        children: [
          _buildBottomNavigationBar(),
          Expanded(child: _buildPageView())
        ],
      ));
    });
  }

  Widget _buildPageView() {
    return PageView(
      physics: const NeverScrollableScrollPhysics(),
      controller: controller.pageController,
      children: const [MessagePage(), ContactsPage(), FindState(), UserPage()],
    );
  }

  Widget _buildBottomNavigationBar() {
    return NavigationRail(
        elevation: 10, // 阴影
        extended: false, // 展开 折叠
        useIndicator: true, // 指示器
        selectedIconTheme: const IconThemeData(color: Colors.white),
        // labelType: NavigationRailLabelType.all,// 图标 文字 排序
        destinations: const [
          NavigationRailDestination(
              icon: Icon(Icons.drive_eta), label: Text("仪表")),
          NavigationRailDestination(
              icon: Icon(Icons.phone), label: Text("联系人")),
          NavigationRailDestination(icon: Icon(Icons.home), label: Text("首页")),
          NavigationRailDestination(
              icon: Icon(Icons.widgets), label: Text("应用")),
        ],
        selectedIndex: controller.state.selectedPageIndex,
        onDestinationSelected: controller.onClickNavBar,
        trailing: Builder(builder: (BuildContext context) {
          return Column(
            children: [
              Container(
                color: Colors.grey,
                height: 1,
                width: 30,
                margin: const EdgeInsets.all(20),
              ),
              IconButton(
                color: Colors.blue,
                onPressed: () {},
                icon: const Icon(Icons.air),
              ),
              IconButton(
                color: Colors.blue,
                onPressed: () {
                  controller.state.airNum = controller.state.airNum + 1;
                },
                icon: const Icon(Icons.expand_less),
              ),
              Text(
                "${controller.state.airNum}" + "℃",
                style: const TextStyle(
                    color: Colors.blue,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    fontFamily: "Harmoney SC"),
              ),
              IconButton(
                color: Colors.blue,
                onPressed: () async {
                  controller.state.airNum = controller.state.airNum - 1;
                },
                icon: const Icon(Icons.expand_more),
              ),
            ],
          );
        }));
  }
}

/*
BottomNavigationBar(
      backgroundColor: Colors.white,
      type: BottomNavigationBarType.fixed,
      currentIndex: controller.state.selectedPageIndex,
      selectedFontSize: AppTheme.smallFont,
      unselectedFontSize: AppTheme.smallFont,
      items: const [
        BottomNavigationBarItem(
          icon: Icon(Ionicons.chatbox_ellipses_outline),
          label: "消息",
        ),
        BottomNavigationBarItem(
          icon: Icon(Ionicons.people_outline),
          label: "联系人",
        ),
        BottomNavigationBarItem(
          icon: Icon(Ionicons.barbell),
          label: "发现",
        ),
        BottomNavigationBarItem(
          icon: Icon(Ionicons.person_outline),
          label: "我",
        )
      ],
      onTap: controller.onClickNavBar,
    );

*/
