import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'dart:ui';
import 'package:amap_flutter_map/amap_flutter_map.dart';
import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:flutter_hmi/common/widget/easy_refresh/custom_classic.dart';
import 'package:flutter_hmi/common/widget/image/image.dart';
import 'package:flutter_hmi/pages/application/pages/user/logic.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

class FindState extends StatefulWidget {
  const FindState({super.key});

  @override
  State<StatefulWidget> createState() => _findStatePage();
}

class _findStatePage extends State<FindState>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black,
      child: PageView(
        scrollDirection: Axis.horizontal,
        controller: PageController(viewportFraction: 0.5),
        children: [music(), map(), air()],
      ),
    );
  }

  Container music() {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 30, 10, 30),
      decoration: BoxDecoration(color: Colors.green),
      child: Column(
        children: [
          const Text(
            "稻香",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          const SizedBox(
            height: 20,
          ),
          ImageFiltered(
            imageFilter: ImageFilter.blur(
                tileMode: TileMode.decal, sigmaX: 1, sigmaY: 1),
            child: ClipPath.shape(
                shape: const CircleBorder(),
                child: const ImageWidget(
                  path:
                      "https://pic2.zhimg.com/80/v2-d3c188ebd6a806d6eee2d2affe59912d_720w.webp",
                  size: 200,
                )),
          )
              .animate(
                onPlay: (controller) => controller.repeat(),
              )
              .rotate(
                  begin: 0,
                  end: 1,
                  duration: Duration(seconds: 4),
                  curve: Curves.linear),
          const SizedBox(
            height: 20,
          ),
          Slider(
              value: 1,
              label: "1：30",
              thumbColor: Colors.white,
              activeColor: Colors.white,
              onChanged: (double) {
                setState(() {});
              }),
          const Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Icon(
              Icons.fast_rewind_outlined,
              size: 40,
              color: Colors.white,
            ),
            Icon(
              Icons.pause,
              size: 40,
              color: Colors.white,
            ),
            Icon(
              Icons.fast_forward_outlined,
              size: 40,
              color: Colors.white,
            ),
          ])
        ],
      ),
    );
  }

  InteractiveViewer map() {
    const mapUrl =
        'https://uploadstatic.mihoyo.com/ys-obc/2022/01/05/75379475/d258137dc0e84fc8acbf77b7dc7115da_1941568151557226408.jpeg?x-oss-process=image/format,webp';
    const mapSize = [4096.0, 4096.0];
    const mapOrigin = [1849.0, 1779.0];
    final marker = jsonDecode(
        '{"id":3,"name":"传送锚点","icon":"https://uploadstatic.mihoyo.com/ys-obc/2022/11/25/75379475/7d650757c10b70cb42189f7fca0c9778_7547860493386240880.png","children":[],"points":[{"id":21594,"x":889.8559699306456,"y":-803.7845201911994},{"id":21593,"x":-662.8577760502653,"y":-1235.21133097975},{"id":21592,"x":242,"y":1063.5},{"id":21591,"x":605.2144440125662,"y":-306.6422600955998},{"id":21590,"x":334.92853900930504,"y":-196.14226009560002},{"id":21589,"x":505.85701697812874,"y":-152.9288852030761},{"id":21588,"x":569.714033956257,"y":38.786609856600535},{"id":21587,"x":-277.69813182939333,"y":807.4834479069223},{"id":21586,"x":174.07146099069496,"y":370.5690403823987},{"id":21585,"x":3.1429219813896907,"y":459.85770940264865},{"id":21584,"x":-187.78587448302028,"y":608.286564103973},{"id":21583,"x":-253.42891854537356,"y":390.78659460572453},{"id":21582,"x":-172.07143047045383,"y":166.07114529867567},{"id":21581,"x":99.78555598743378,"y":14.5},{"id":21580,"x":-92.92847796882324,"y":-5.5},{"id":21579,"x":-179.71441349232555,"y":-101.85566548987526},{"id":21578,"x":-86.35742703443793,"y":-151.78659460572453},{"id":21577,"x":-433.2130479492562,"y":-355.2948922638252},{"id":21576,"x":-522.7854949469518,"y":-201.42882419957277},{"id":21575,"x":-656.4285084890644,"y":-293.85566548987526},{"id":21574,"x":-648.6429219813897,"y":-67.28452019119959},{"id":21573,"x":-927.0714609906948,"y":135.21334439077236},{"id":21572,"x":-1379.35707801861,"y":382.6422600955998},{"id":21571,"x":-1204.857427034438,"y":730.5}]}');
    const markerSize = 32.0;

    final transformation = TransformationController();
    final StreamController<double> stream = StreamController.broadcast();

    @override
    void initState() {
      super.initState();
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        final size = MediaQuery.of(context).size;
        transformation.value.scale(
          max(size.width / mapSize[0], size.height / mapSize[1]),
        );
        setState(() {});
      });
      transformation.addListener(() {
        stream.sink.add(transformation.value[0]);
      });
    }

    return InteractiveViewer(
      transformationController: transformation,
      constrained: false,
      maxScale: 1.5,
      minScale: 0.1,
      child: SizedBox(
        width: 400,
        height: MediaQuery.of(context).size.height,
        child: Stack(children: [
          const Image(image: NetworkImage(mapUrl)),
          ...marker['points'].map(
            (i) => Positioned(
              left: i['x'] + mapOrigin[0] - markerSize / 2,
              top: i['y'] + mapOrigin[1] - markerSize,
              width: markerSize,
              height: markerSize,
              child: StreamBuilder(
                stream: stream.stream,
                builder: (context, snapshot) {
                  final scale = snapshot.data ?? transformation.value[0];
                  return Transform.scale(
                    scale: 1 / scale,
                    alignment: Alignment.bottomCenter,
                    child: CupertinoButton(
                      padding: EdgeInsets.zero,
                      onPressed: () {
                        ScaffoldMessenger.of(context)
                          ..clearSnackBars()
                          ..showSnackBar(
                            SnackBar(content: Text('onPressed ${i['id']}')),
                          );
                      },
                      child: Image.network(marker['icon']),
                    ),
                  );
                },
              ),
            ),
          ),
        ]),
      ),
    );
  }

  Container air() {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 30, 10, 30),
      decoration: BoxDecoration(color: Colors.blue),
      child: Column(
        children: [
          Text(
            "20C" + " 小雪",
            style: TextStyle(
                color: Colors.white, fontSize: 20, fontWeight: FontWeight.w500),
          ),
          Text(
            "哈尔滨",
            style: TextStyle(
                color: Colors.white, fontSize: 20, fontWeight: FontWeight.w400),
          )
        ],
      ),
    );
  }
}


/*
import 'dart:async';
import 'dart:convert';

import 'package:audioplayers/audioplayers.dart';
import 'package:blur/blur.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class suijiyinyuetuijian extends StatefulWidget {
  const suijiyinyuetuijian({super.key});

  @override
  State<suijiyinyuetuijian> createState() => _suijiyinyuetuijianState();
}

class _suijiyinyuetuijianState extends State<suijiyinyuetuijian> {
  AudioPlayer audioPlayer = AudioPlayer();
  bool _ispauseMusic = true; //是否暂停播放标识
  bool _isfirstplay = true; //是否是第一次播放音乐标识
  var _maxtime = 1.0; //进度条最大长度
  var _musicjindu = 0.0; //进度条初始长度
  var _netjson; //页面构建所需的json
  var _maxtime_xianshi = "0:00"; //显示的总长度
  var _musicjindu_xianshi = "0:00"; //进度显示

  late Future _netmusic; //中转函数

  var name; //歌曲的名字
  var auther; //歌曲的作者
  var picUrl; //歌曲的图片
  var mp3url; //歌曲的链接
  var _content; //歌曲的热评
  var _zhanweitupian = 'images/mohuse.webp'; //占位图设置
  var _isfail = false;
  List yinyuehuancun = []; //创建临时音乐缓存字典
  var _huancun_xiaobiaojianshu = 2; //缓存下表减少数
  bool _shangyishou_key = false; //标注是否点击了上一首
  bool _bofangshunxu = true; // 播放的顺序按钮切换标识
  var _time_daojishi = 3; // 倒计时

  @override
  void initState() {
    // TODO: implement initState
    _netmusic = _net();
    super.initState();
  }

// 切换上一首歌曲
  Future<void> _shangyishou() async {
    // 获取临时缓存的音乐json数据
    var changdu = yinyuehuancun.length; //获取缓存长度
    if (changdu < 2) {
      print("缓存长度不够");
    } else {
      if (_shangyishou_key == false) {
        _shangyishou_key = true;
        _isfail = false;
        int i = changdu - _huancun_xiaobiaojianshu; //标注缓存倒数下标
        print('获取的缓存数据${yinyuehuancun[i]}');
        setState(() {
          _isfail = false;
          _isfirstplay = true;
          _ispauseMusic = true;
          _musicjindu = 0.0;
          _seekTo(0);
          _netjson = yinyuehuancun[i];
        });
        _huancun_xiaobiaojianshu = _huancun_xiaobiaojianshu + 1;
      } else if (_shangyishou_key == true) {
        _shangyishou_key = true;
        _isfail = false;
        int i = changdu - _huancun_xiaobiaojianshu; //标注缓存倒数下标
        print('获取的缓存数据${yinyuehuancun[i]}');
        setState(() {
          _isfail = false;
          _isfirstplay = true;
          _ispauseMusic = true;
          _musicjindu = 0.0;
          _seekTo(0);
          _netjson = yinyuehuancun[i];
        });
        _huancun_xiaobiaojianshu = _huancun_xiaobiaojianshu + 1;
      }
    }
  }

//获取歌曲信息
  Future<void> _huoquxinxi(_mp3url) async {
    try {
      await audioPlayer.setSourceUrl(_mp3url);
      Duration? maxDuration = await audioPlayer.getDuration();
      _maxtime = maxDuration!.inMilliseconds.toDouble();
      setState(() {
        var a = _maxtime / 1000;
        var d = a.toInt();
        var c = Duration(minutes: d).toString().substring(0, 4);
        _maxtime_xianshi = c;
      });
    } catch (e) {
      _isfirstplay = true;
      _ispauseMusic = true;
      _musicjindu = 0.0;
      _seekTo(0);
      print("获取歌曲长度失败");
    }
  }

  void _anniucaozuo() {
    //播放按钮的操作
    if (_ispauseMusic == false && _isfirstplay == false) {
      _pauseMusic(); //暂停播放
      setState(() {
        _ispauseMusic = !_ispauseMusic;
      });
    } else if (_ispauseMusic == true && _isfirstplay == false) {
      _resumeMusic(); //恢复播放
      setState(() {
        _ispauseMusic = !_ispauseMusic;
      });
    } else if (_isfirstplay == true) {
      _playMusic(mp3url); //开始播放
      _isfirstplay = false;
    }
  }

//歌歌曲切换操作
  Future<void> _music_qiehuan() async {
    try {
      var key = yinyuehuancun.length - 1;
      var key0 = yinyuehuancun.length - _huancun_xiaobiaojianshu + 1;
      var _id1 =
          jsonDecode(yinyuehuancun[key].toString())['data']['id']; // 获取缓存最后的id
      var _id2 = jsonDecode(yinyuehuancun[key0].toString())['data']['id'];
      print(
          "id1=$_id1加${jsonDecode(yinyuehuancun[key].toString())['data']},id2=$_id2加${jsonDecode(yinyuehuancun[key0].toString())['data']}");
      if (_id1 == _id2) {
        _huancun_xiaobiaojianshu = 2;
        await _net();
        await Future.delayed(Duration(milliseconds: 1800)); //等待1.8秒
        setState(() {
          var json = jsonDecode(_netjson.toString())['data'];
          name = json['name'];
          auther = json['auther'];
          picUrl = json['picUrl'];
          mp3url = json['mp3url'];
          _content = json['content'];
          _huoquxinxi(mp3url);
        });
      } else {
        print("使用缓存操作");
        if (_shangyishou_key == false) {
          _huancun_xiaobiaojianshu = _huancun_xiaobiaojianshu - 1;
          var i = yinyuehuancun.length - _huancun_xiaobiaojianshu;
          setState(() {
            _isfail = false;
            _isfirstplay = true;
            _ispauseMusic = true;
            _musicjindu = 0.0;
            _musicjindu_xianshi = '0:00';
            _seekTo(0);
            _netjson = yinyuehuancun[i];
          });
        } else if (_shangyishou_key == true) {
          _huancun_xiaobiaojianshu = _huancun_xiaobiaojianshu - 2;
          _shangyishou_key = false;
          var i = yinyuehuancun.length - _huancun_xiaobiaojianshu;
          setState(() {
            _isfail = false;
            _isfirstplay = true;
            _ispauseMusic = true;
            _musicjindu = 0.0;
            _seekTo(0);
            _netjson = yinyuehuancun[i];
          });
        }
      }
    } catch (e) {
      print("下一首歌异常处理");
      _huancun_xiaobiaojianshu = 2;
      setState(() {
        _isfail = false;
        _isfirstplay = true;
        _ispauseMusic = true;
        _musicjindu = 0.0;
        _seekTo(0);
      });
      await _net();
    }
  }

//获取网易云热门音乐json数据
  Future<void> _net() async {
    try {
      Dio dio = Dio();
      Response response = await dio.get('https://api.vvhan.com/api/reping');
      if (response.statusCode == 200) {
        _netjson = response;
        yinyuehuancun.add(response);
        print("获取的网络数据$_netjson");
        print("获取的缓存数据$yinyuehuancun");
      }
    } catch (e) {
      debugPrint('获取网易云音乐数据失败$e');
    }
  }

  void _resumeMusic() async {
    //恢复播放状态
    audioPlayer.resume();
    debugPrint("恢复播放音乐");
  }

  void _playMusic(_musicurl) async {
    //播放音乐
    debugPrint("开始播放音乐0");
    if (_musicurl == 'https://music.163.com/404') {
      setState(() {
        _isfail = true;
      });
      Timer.periodic(Duration(milliseconds: 1000), (timer) async {
        setState(() {
          _time_daojishi = _time_daojishi - 1;
        });
        if (_time_daojishi == 0) {
          setState(() {
            _isfail = false;
          });
          try {
            audioPlayer.dispose();
            audioPlayer = AudioPlayer();
            _isfail = false;
            _ispauseMusic = true;
            setState(() {
              _musicjindu_xianshi = "0:00";
              _musicjindu = 0.0;
              _isfirstplay = true;
            });
          } catch (e) {
            print("下一首异常：摧毁对象失败");
          }
          await _music_qiehuan();
          _anniucaozuo();
          _time_daojishi = 3;
          timer.cancel(); // 关闭计时器
        }
      });
    } else {
      await audioPlayer.play(UrlSource(_musicurl));
      // 获取音乐的总时长
      try {
        Duration? maxDuration = await audioPlayer.getDuration();
        _maxtime = maxDuration!.inMilliseconds.toDouble();
        print("音乐时常：$_maxtime");
        setState(() {
          var a = _maxtime / 1000;
          var d = a.toInt();
          var c = Duration(minutes: d).toString().substring(0, 4);
          _maxtime_xianshi = c;
        });
        //获取音乐进度
        await audioPlayer.onPositionChanged.listen((event) {
          setState(() {
            _musicjindu = event.inMilliseconds.toDouble();
            var b = _musicjindu / 1000;
            var d = b.toInt();
            var c = Duration(minutes: d).toString().substring(0, 4);
            _musicjindu_xianshi = c;
          });
          debugPrint("进度$_musicjindu_xianshi");
        });
        debugPrint("开始播放音乐$maxDuration");
        setState(() {
          _ispauseMusic = !_ispauseMusic;
        });
      } catch (e) {
        await Future.delayed(Duration(seconds: 3));
        try {
          audioPlayer.dispose();
          audioPlayer = AudioPlayer();
        } catch (e) {
          print("下一首异常：摧毁对象失败");
        }
        await _music_qiehuan();
      }
      //音乐播放完毕的操作
      audioPlayer.onPlayerComplete.listen((event) async {
        if (_bofangshunxu == true) {
          // 如果是单曲循环则执行这里
          setState(() {
            _isfail = false;
            _musicjindu = 0.0;
            _musicjindu_xianshi = "0:00";
          });
          await audioPlayer.play(UrlSource(_musicurl)); // 播放音乐
        } else if (_bofangshunxu == false) {
          // 随机播放执行这里
          await Future.delayed(Duration(seconds: 2));
          try {
            audioPlayer.dispose();
            audioPlayer = AudioPlayer();
            _isfail = false;
            _ispauseMusic = true;
            setState(() {
              _musicjindu_xianshi = "0:00";
              _musicjindu = 0.0;
              _isfirstplay = true;
            });
          } catch (e) {
            print("下一首异常：摧毁对象失败");
          }
          await _music_qiehuan();
          _anniucaozuo();
        }
      });
    }
  }

  void _pauseMusic() async {
    //暂停音乐
    await audioPlayer.pause();
    debugPrint("暂停播放音乐");
  }

  void _seekTo(double milliseconds) async{
    //拖动音乐
    var key = milliseconds / 1000;
    audioPlayer.seek(Duration(seconds: key.toInt()));
    await audioPlayer.onPositionChanged.listen((event) {
      setState(() {
        _musicjindu = event.inMilliseconds.toDouble();
        var b = _musicjindu / 1000;
        var d = b.toInt();
        var c = Duration(minutes: d).toString().substring(0, 4);
        _musicjindu_xianshi = c;
      });
      debugPrint("进度$_musicjindu_xianshi");
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: WillPopScope(
          child: FutureBuilder(
            future: _netmusic,
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                // 异步操作正在进行中，显示加载指示器
                return Center(
                  child: CircularProgressIndicator(),
                );
              } else if (snapshot.hasError) {
                // 异步操作出错，显示错误信息
                return Text('Error: ${snapshot.error}');
              } else {
                // 异步操作已完成，显示数据
                var json = jsonDecode(_netjson.toString())['data'];
                name = json['name'];
                auther = json['auther'];
                picUrl = json['picUrl'];
                mp3url = json['mp3url'];
                _content = json['content'];
                _huoquxinxi(mp3url);
                return Stack(
                  children: [
                    Blur(
                        blur: 12,
                        blurColor: Colors.transparent,
                        colorOpacity: 0.2,
                        child: Container(
                          height: double.infinity,
                          child: CachedNetworkImage(
                              fit: BoxFit.cover,
                              imageUrl: picUrl,
                              httpHeaders: {
                                'User-Agent':
                                    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36 Edg/117.0.2045.55'
                              },
                              placeholder: (context, url) => Image.asset(
                                    _zhanweitupian,
                                    fit: BoxFit.cover,
                                  )),
                        )),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(20, 20, 20, 20),
                          child: Column(
                            children: [
                              Container(
                                child: Text(
                                  '$name',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white54,
                                      fontSize: 32,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Container(
                                child: Text(
                                  "$auther",
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.white60),
                                ),
                              ),
                            ],
                          ),
                        ),
                        //大图片
                        Stack(
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15)),
                              child: Blur(
                                borderRadius: BorderRadius.circular(15),
                                blur: 0.5,
                                blurColor: Colors.grey,
                                colorOpacity: 0.2,
                                child: Container(
                                  height: 300,
                                  width: 310,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(15),
                                    child: CachedNetworkImage(
                                        matchTextDirection: true,
                                        fit: BoxFit.cover,
                                        imageUrl: picUrl,
                                        httpHeaders: {
                                          'User-Agent':
                                              'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36 Edg/117.0.2045.55'
                                        },
                                        placeholder: (context, url) =>
                                            Image.asset(
                                              _zhanweitupian,
                                              fit: BoxFit.cover,
                                              matchTextDirection: true,
                                            )),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                        //评论
                        Container(
                            margin: EdgeInsets.fromLTRB(12, 10, 12, 0),
                            child: Text(
                              "$_content",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.white54,
                              ),
                            )),

                        //进度条
                        Container(
                          margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Container(
                                  width: 35,
                                  child: Text(
                                    "$_musicjindu_xianshi",
                                    style: TextStyle(color: Colors.white54),
                                  )),
                              Expanded(
                                flex: 1,
                                child: Slider(
                                    activeColor: Colors.blueGrey,
                                    min: 0,
                                    max: _maxtime + 900,
                                    value: _musicjindu,
                                    onChanged: (e) {
                                      setState(() {
                                        _musicjindu = e;
                                        print("滑动值${e.toInt()}");
                                        _seekTo(e);
                                      });
                                    }),
                              ),
                              Text(
                                "$_maxtime_xianshi",
                                style: TextStyle(color: Colors.white54),
                              )
                            ],
                          ),
                        ),
                        //控制器的面板
                        Container(
                            //播放顺序控制（随机播放。单曲循环）
                            child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              margin: EdgeInsets.fromLTRB(0, 18, 0, 0),
                              height: 62,
                              width: 62,
                              child: IconButton(
                                onPressed: () {
                                  _bofangshunxu = !_bofangshunxu;
                                },
                                icon: _bofangshunxu
                                    ? Icon(
                                        Icons.repeat_one,
                                        size: 46,
                                        color: Colors.white54,
                                      )
                                    : Icon(
                                        Icons.shuffle,
                                        size: 42,
                                        color: Colors.white54,
                                      ),
                              ),
                            ),
                            SizedBox(
                              height: 62,
                              width: 62,
                              child: IconButton(
                                  onPressed: () {
                                    try {
                                      audioPlayer.stop();
                                      audioPlayer.dispose();
                                      audioPlayer = AudioPlayer();
                                    } catch (e) {
                                      print("上一首异常：摧毁对象失败");
                                    }
                                    _isfail = false;
                                    _ispauseMusic = true;
                                    _shangyishou(); //上一首
                                  },
                                  icon: Icon(
                                    Icons.skip_previous_rounded,
                                    size: 62,
                                    color: Colors.white54,
                                  )),
                            ),
                            Container(
                                padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                                child: SizedBox(
                                  height: 62,
                                  width: 62,
                                  child: Container(
                                      child: IconButton(
                                          onPressed: _anniucaozuo,
                                          icon: _ispauseMusic
                                              ? Icon(
                                                  Icons.play_circle_rounded,
                                                  size: 62,
                                                  color: Colors.white54,
                                                )
                                              : Icon(
                                                  Icons.pause,
                                                  size: 62,
                                                  color: Colors.white54,
                                                ))),
                                )),
                            SizedBox(
                              height: 62,
                              width: 62,
                              child: IconButton(
                                  onPressed: () {
                                    try {
                                      audioPlayer.dispose();
                                      audioPlayer = AudioPlayer();
                                      _isfail = false;
                                      _ispauseMusic = true;
                                      setState(() {
                                        _musicjindu_xianshi = "0:00";
                                        _musicjindu = 0.0;
                                        _isfirstplay = true;
                                      });
                                    } catch (e) {
                                      print("下一首异常：摧毁对象失败");
                                    }
                                    _music_qiehuan();
                                  },
                                  icon: Icon(
                                    Icons.skip_next_rounded,
                                    size: 62,
                                    color: Colors.white54,
                                  )),
                            ),
                            Container(
                              // 展开播放目录列表
                              margin: EdgeInsets.fromLTRB(10, 17, 0, 0),
                              height: 62,
                              width: 62,
                              child: IconButton(
                                onPressed: () {},
                                icon: Icon(
                                  Icons.queue_music,
                                  size: 46,
                                  color: Colors.white54,
                                ),
                              ),
                            ),
                          ],
                        )),
                        Container(
                            //歌曲加载失败显示的页面
                            margin: EdgeInsets.all(15),
                            child: _isfail
                                ? Text(
                                    "未找到歌曲信息:$_time_daojishi",
                                    style: TextStyle(color: Colors.white54),
                                  )
                                : Container())
                      ],
                    )
                  ],
                );
              }
            },
          ),
          onWillPop: () async {
            // 拦截返回操作（暂停音乐后再退出）
            audioPlayer.stop();
            return true;
          }),
    );
  }
}


*/