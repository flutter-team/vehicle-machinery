import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:flutter_hmi/common/style/theme.dart';
import 'package:flutter_hmi/common/utils/dio/request.dart';
import 'package:flutter_hmi/common/widget/cell/cell.dart';
import 'package:flutter_hmi/common/widget/image/image.dart';
import 'package:flutter_hmi/pages/application/pages/contacts/view.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

import 'logic.dart';

var listData = [];

// ignore: must_be_immutable
class UserPage extends StatefulWidget {
  const UserPage({super.key});

  @override
  State<StatefulWidget> createState() => _UsePageState();
}

class _UsePageState extends State<UserPage> {
  @override
  void initState() {
    super.initState();
    getData();
  }

  void getData() async {
    SMDioManager().dio?.options.baseUrl = BaseUrl;
    final res = await SMDioManager.post(path: "/hmi/menu");
    setState(() {
      listData = res["data"]["list"];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: const BoxDecoration(color: Colors.black),
      child: GridView.count(
          crossAxisCount: 6,
          scrollDirection: Axis.vertical,
          children: List.generate(listData.length, (index) {
            return GestureDetector(
              onTap: () {
                print("点击$index");
              },
              child: Container(
                padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                decoration: const BoxDecoration(color: Colors.transparent),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    ImageWidget(
                      path: listData[index]["icon"],
                      radius: 8,
                      size: 50,
                    ),
                    Text(
                      listData[index]['name'],
                      style: const TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ).animate().fade(duration: 600.ms).scale(),
            );
          })),
    );
  }
}
