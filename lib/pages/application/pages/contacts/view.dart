import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hmi/common/utils/dio/request.dart';

import '../../../../common/style/theme.dart';
import '../../../../common/widget/easy_refresh/custom_classic.dart';
import '../../../../common/widget/image/image.dart';

var dataList = [];
var pageNum = 1;

class ContactsPage extends StatefulWidget {
  const ContactsPage({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _ContactsPageState createState() => _ContactsPageState();
}

class _ContactsPageState extends State<ContactsPage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getdata();
  }

  void getdata() async {
    SMDioManager().dio?.options.baseUrl = BaseUrl;
    final res = await SMDioManager.post(path: "/friend/list");

    setState(() {
      if (pageNum == 1) {
        dataList = res['data']['list'];
      } else {
        dataList.addAll(res['data']['list']);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        bottomOpacity: 0,
        backgroundColor: Colors.transparent,
        title: Container(
          height: 40,
          padding: const EdgeInsets.all(0),
          child: const SearchBar(
            hintText: "请输入搜索内容",
            shadowColor: MaterialStatePropertyAll(Colors.transparent),
          ),
        ),
      ),
      body: Builder(builder: (BuildContext context) {
        return WillPopScope(
            child: EasyRefresh(
                header: CustomClassic().getHeader(),
                footer: CustomClassic().getFooter(),
                onLoad: () {
                  pageNum++;
                  getdata();
                },
                onRefresh: () {
                  pageNum = 1;
                  getdata();
                },
                child: ListView.builder(
                    itemCount: dataList.length,
                    itemBuilder: (context, index) {
                      return Container(
                        margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                        decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(6)),
                            color: Colors.white),
                        child: ListTile(
                          dense: true,
                          contentPadding: const EdgeInsets.all(10),
                          title: Text(dataList[index]["nickName"]),
                          leading: ImageWidget(
                            path: dataList[index]["avatar"],
                            size: 45,
                            radius: AppTheme.radius,
                          ),
                        ),
                      );
                    })),
            onWillPop: () {
              return Future(() => false);
            });
      }),
    );
  }
}
