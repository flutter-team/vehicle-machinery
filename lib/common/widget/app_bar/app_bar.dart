import 'package:flutter/material.dart';

AppBar customAppBar(
  String title, {
  List<Widget>? actions,
}) {
  return AppBar(
    centerTitle: true,
    title: Text(title),
    actions: actions,
  );
}
