import 'dart:developer';

import 'package:flutter/foundation.dart';

class LogUtils {
  /// 是否敞开日志，默许Debug形式下敞开
  static bool isOpenLog = kDebugMode;

  /// 调试打印
  static void println(String obj) {
    if (isOpenLog) {
      print(obj);
    }
  }

  /// Log 用于网络请求等长内容日志
  static void logger(String obj, {StackTrace? stackTrace, int level = 0}) {
    if (isOpenLog) log(obj, stackTrace: stackTrace, level: level);
  }
}
