class StringUtils {
  /// 判断是否为null
  /// null -> true
  /// not null -> false
  static bool hasEmpty(String? text) {
    return text == null || text.isEmpty;
  }
}
